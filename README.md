# Hued.me test assignment

## Concepts

> See http://localhost:3000/api

* TypeScript
* NestJS
* Docker
* OpenAPI/Swagger
* in-memory caching

### Solutions used

#### Geocoding

`@goparrot/geocoder`, library providing unified interface for various geocoding providers. It also provides
**chaining of providers** and the logic to request next provider in case the previous one responded with
no result or failed for any reason.

Custom provider (minimal implementation) for OpenStreetMaps/Nominatim has been written by me atop of corresponding
interfaces from `@goparrot/geocoder`.

#### GeoJSON

`turf` libraries have been used for matching `point-in-polygon`. A coordinates point is checked against
a polygon (GeoJSON `features`).

**Alternative solution** would be to use the geo-indexed databased (eg MongoDB, RethinkDB) and perform
`point-in-polygon` query using built-in database features.

If the `turf` library could have not been used, the algorithm to match point-in-polygon would be the following:

1. Find all polygon edges intersecting with point meridian and parallel.
2. Find the intersection points for found polygon edges and point **meridian**.
3. Sort the intersection points from west to east.
4. If the address point lies between even-indexed and odd-indexed intersection point, it is located within the polygon. Otherwise, (between odd-indexed and even-indexed intersection point), it is not.

#### Streaming GeoJSON

As I used the GeoJSON file, I introduced stream parsing for performance (`stream-json` library).

#### Caching

There is a minimal implementation for caching provided (in-memory). Due to the time limitations, both
geocoding responses and service area GeoJSON matches were not wrapped as `Observables`, which would enable
the developers to use built-in NestJS interceptors for dynamic caching pipeline implementation.

#### Validation

A simple validation logic was implemented to ensure that the `address` field posted as a JSON body is minimum
three characters long (built-in NestJS validation pipeline with `class-validator`).

## Usage

### Installation

1. Copy the `.env.example` as `.env` (for this task reasons, the example `.env` file is the same as its final form, but `.env` files as such should never be pushed to the repositories).
2. Copy the GeoJSON file to the `assets` directory and check whether is name matched the name from the `.env` file.
3. `npm install`

### Running

```shell
docker-compose up prod
docker-compose up dev
```

Alternatively, Nest CLI command can be used, such as:

```shell
npm run start
npm run start:dev
```

### Testing

```shell
npm run test     # for integration (feature) and system (unit) tests
npm run test:cov # for code coverage
npm run test:e2e # for end-to-end (acceptance) tests
```

`AreaController` has not been tested otherwise than with E2E tests.

`OpenStreetMapsProvider` has not been tested as it would require time-consuming API mocking, besides, the
Nominatim API is quite simple and self-explanatory. 

## Things to consider

1. What to do with the points that are either node vertices of a GeoJSON polygon, or are 100% matching with
    polygon edge? Which service area should they fall into?
2. Wrap geocoding and `point-in-polygon` matching with `Observables` to implement built-in NestJS interceptors.
3. Better validation for user input could rely on autocomplete logic for addresses. 
4. Currently, if address does not fall into any of the known service areas, it is not returned within the
    final response (`NO_SERVICE_AREA` without `location`). This might be reconsidered.
5. The task instruction assumed the final response to contain `address1` and `address2` fields (so they include
    well-formatted street, house number, and other corresponding values). While it is generally doable, one should
    consider the fact that each country might have its standards for street addresses formatting (`{street} {house}` or
    `{house} {street}`). I have not implemented it, as OSM returns street and house number separately, and the formatted
    address there happen to look awkward-ish, but there are several libraries for such (eg [@fragaria/address-formatter](https://www.npmjs.com/package/@fragaria/address-formatter)).
6. Another thing to consider here is resolving the _city_ name, as - depending on local quirks - _city_ might refer
    to _city_, _city_district_, _village_ or even (eg in Poland) the nearest post office.
