import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('AreaController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  it('/area (POST) should return 400 validation error when address is too short', () => {
    return request(app.getHttpServer())
      .post('/area')
      .send({ address: '12' })
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .expect(400, {
        statusCode: 400,
        message: ['address must be longer than or equal to 3 characters'],
        error: 'Bad Request',
      });
  });

  it('/area (POST) should return valid result when address is found and it is with service area', () => {
    return request(app.getHttpServer())
      .post('/area')
      .send({ address: '23 como road london uk' })
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .expect(200, {
        status: 'OK',
        search: '23 como road london uk',
        location: {
          address1: 'Como Road',
          postcode: 'SE23 2JW',
          serviceArea: 'LONSOUTHEAST-EXT',
          lng: '-0.0434693',
          lat: '51.4370161',
          city: 'London',
        },
      });
  });

  it('/area (POST) should return ADDRESS_NOT_FOUND when address does not exist', () => {
    return request(app.getHttpServer())
      .post('/area')
      .send({ address: 'THIS ADDRESS NEVER EXISTED, VENUS, UNIVERSE' })
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .expect(200, {
        status: 'ADDRESS_NOT_FOUND',
        search: 'THIS ADDRESS NEVER EXISTED, VENUS, UNIVERSE',
      });
  });

  it('/area (POST) should return NO_SERVICE_AREA when address exists but outside any service area', () => {
    return request(app.getHttpServer())
      .post('/area')
      .send({ address: 'Sopot, Polska' })
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .expect(200, {
        status: 'NO_SERVICE_AREA',
        search: 'Sopot, Polska',
      });
  });
});
