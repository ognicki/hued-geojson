import { AbstractHttpProvider } from '@goparrot/geocoder';
import { AxiosInstance } from 'axios';
import { OpenStreetMapsGeocodeCommand } from './command/geocode.command';
import { OpenStreetMapsReverseCommand } from './command/reverse.command';

/**
 * OpenStreetMaps provider with minimal implementation
 */
export class OpenStreetMapsProvider extends AbstractHttpProvider {
  constructor(httpClient: AxiosInstance) {
    super({
      geocode: new OpenStreetMapsGeocodeCommand(httpClient),
      reverse: new OpenStreetMapsReverseCommand(httpClient),
    });
  }
}
