import { ReverseCommand } from '@goparrot/geocoder';

/**
 * Required by GeocodeProvider (not used)
 */
export class OpenStreetMapsReverseCommand extends ReverseCommand {}
