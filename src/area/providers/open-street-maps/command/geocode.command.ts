import { OpenStreetMapsGeocodeTransformer } from '../transformer/geocode.transformer';
import { OpenStreetMapsProvider } from '../open-street-maps.provider';
import {
  GeocodeCommand,
  GeocodeQueryInterface,
  Location,
} from '@goparrot/geocoder';

/**
 * Minimal implementation for OpenStreetMaps geocode command
 */
export class OpenStreetMapsGeocodeCommand extends GeocodeCommand {
  /**
   * Get base URL for OSM
   */
  static getUrl(): string {
    return 'https://nominatim.openstreetmap.org/search';
  }

  /**
   * Build query (return required params)
   * @param query
   */
  async buildQuery(
    query: GeocodeQueryInterface,
  ): Promise<{ q: string; format: string; addressdetails: number }> {
    return {
      q: query.address,
      format: 'json',
      addressdetails: 1,
    };
  }

  /**
   * Execute query
   * @param query
   */
  async execute(query: GeocodeQueryInterface): Promise<Location[]> {
    const params = await this.buildQuery(query);

    let data: Array<Record<string, any>>;

    try {
      data = (
        await this.httpClient.get(OpenStreetMapsGeocodeCommand.getUrl(), {
          params,
        })
      ).data;

      return await Promise.all(
        data.map((item) =>
          new OpenStreetMapsGeocodeTransformer(
            OpenStreetMapsProvider,
            item,
          ).transform(),
        ),
      );
    } catch (_err) {
      return [];
    }
  }
}
