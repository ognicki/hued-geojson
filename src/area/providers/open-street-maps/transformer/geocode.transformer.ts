import {
  AbstractLocationTransformer,
  LocationInterface,
} from '@goparrot/geocoder';
import { OpenStreetMapsProvider } from '../open-street-maps.provider';
import { get } from 'lodash';

/**
 * Minimal geocode transformer implementation for OpenStreetMaps provider
 */
export class OpenStreetMapsGeocodeTransformer extends AbstractLocationTransformer<OpenStreetMapsProvider> {
  getCity(): Promise<LocationInterface['city']> {
    return get(
      this.raw,
      'address.city',
      get(this.raw, 'address.city_district', get(this.raw, 'address.village')),
    );
  }

  getCountry(): Promise<LocationInterface['country']> {
    return get(this.raw, 'address.country');
  }

  getCountryCode(): Promise<LocationInterface['countryCode']> {
    return get(this.raw, 'address.country_code');
  }

  getFormattedAddress(): Promise<LocationInterface['formattedAddress']> {
    return get(this.raw, 'display_name');
  }

  getHouseNumber(): Promise<LocationInterface['houseNumber']> {
    return get(this.raw, 'address.house_number');
  }

  getLatitude(): Promise<LocationInterface['latitude']> {
    return get(this.raw, 'lat');
  }

  getLongitude(): Promise<LocationInterface['longitude']> {
    return get(this.raw, 'lon');
  }

  getPlaceId(): Promise<LocationInterface['placeId']> {
    return get(this.raw, 'place_id');
  }

  getPostalCode(): Promise<LocationInterface['postalCode']> {
    return get(this.raw, 'address.postcode');
  }

  getState(): Promise<LocationInterface['state']> {
    return get(this.raw, 'address.state_district');
  }

  getStateCode(): Promise<LocationInterface['stateCode']> {
    return Promise.resolve(undefined);
  }

  getStreetName(): Promise<LocationInterface['streetName']> {
    return get(this.raw, 'address.road');
  }
}
