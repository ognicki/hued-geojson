import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { AreaService } from './area.service';
import { GetAreaRequestDto } from './dto/get-area-request.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GetAreaResponseDto } from './dto/get-area-response.dto';
import { ErrorResponse } from '../common/response/error.response';

@ApiTags('Area')
@Controller('area')
export class AreaController {
  constructor(private readonly areaService: AreaService) {}

  @Post()
  @HttpCode(200)
  @ApiOperation({
    summary: 'Find a service area for address',
  })
  @ApiResponse({
    status: 200,
    description: 'Geocode and match result',
    type: GetAreaResponseDto,
  })
  @ApiResponse({
    status: 400,
    description: 'Validation error',
    type: ErrorResponse,
  })
  async getArea(
    @Body() getAreaDto: GetAreaRequestDto,
  ): Promise<ServiceAreaResponse> {
    return await this.areaService.getArea(getAreaDto.address);
  }
}
