import { Test, TestingModule } from '@nestjs/testing';
import { AreaService } from './area.service';
import { omit } from 'lodash';
import { CacheModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import geojsonConfig from '../config/geojson.config';

describe('AreaService', () => {
  let service: AreaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        CacheModule,
        ConfigModule.forRoot({
          load: [geojsonConfig],
        }),
      ],
      providers: [AreaService],
    })
      .useMocker((token) => {
        if (token === 'CACHE_MANAGER') {
          return {
            set: async (...args: any[]) => undefined,
            get: async (...args: any[]) => undefined,
          };
        }
        if (token === 'CACHE_MODULE_OPTIONS') {
          return {};
        }
      })
      .compile();

    service = module.get<AreaService>(AreaService);
  });

  it('should normalize the address', () => {
    const scenarios = [
      {
        address: '23 como     rd, UnItEd Kingdom',
        result: '23 COMO RD, UNITED KINGDOM',
      },
    ];

    for (const scenario of scenarios) {
      expect(service.normalizeAddress(scenario.address)).toEqual(
        scenario.result,
      );
    }
  });

  it('should convert coordinate to number', () => {
    const scenarios = [
      {
        coord: '12.2364734683429',
        result: 12.2364734683429,
      },
      {
        coord: '-13',
        result: -13,
      },
    ];

    for (const scenario of scenarios) {
      expect(service.coordToNumber(scenario.coord)).toEqual(scenario.result);
    }
  });

  it('should convert invalid coordinate to undefined', () => {
    const scenarios = [
      {
        coord: '181',
        result: undefined,
      },
      {
        coord: Infinity,
        result: undefined,
      },
    ];

    for (const scenario of scenarios) {
      expect(service.coordToNumber(scenario.coord)).toEqual(scenario.result);
    }
  });

  it('should return service areas', async () => {
    const scenarios = [
      {
        lat: 51.48707495,
        lng: -2.497590558920596,
        result: 'UK-BRISTOL',
      },
      {
        lat: 51.4370161,
        lng: -0.0434693,
        result: 'LONSOUTHEAST-EXT',
      },
      {
        lat: 51.4370161,
        lng: 22,
        result: undefined,
      },
      /**
       * This is exactly the node point of bounding box for LONCENTRAL
       * @todo What should we do with such cases?
       */
      {
        lat: 51.53189499745622,
        lng: -0.10419183328645,
        result: 'LONCENTRAL',
      },
    ];

    for (const scenario of scenarios) {
      const serviceArea = await service.findServiceArea(
        omit(scenario, ['result']),
        false,
      );
      expect(serviceArea).toEqual(scenario.result);
    }
  }, 20000);

  it('should throw error when coordinates are invalid', async () => {
    const scenarios = [
      {
        lat: -91,
        lng: 180,
        message: 'Latitude cannot be higher than 90',
      },
      {
        lat: -89,
        lng: 181,
        message: 'Both coordinates must be valid numbers',
      },
      {
        lat: 'Infinity',
        lng: 180,
        message: 'Both coordinates must be valid numbers',
      },
    ];

    let message: string, error: boolean;

    for (const scenario of scenarios) {
      error = false;
      message = '';

      try {
        await service.findServiceArea({ lat: scenario.lat, lng: scenario.lng });
      } catch (err) {
        error = true;
        message = err.message;
      }

      console.log({ scenario });
      expect(error).toBeTruthy();
      expect(message).toEqual(scenario.message);
    }
  });
});
