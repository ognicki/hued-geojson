import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import {
  ChainProvider,
  ProviderAggregator,
  Location,
} from '@goparrot/geocoder';
import axios, { AxiosInstance } from 'axios';
import { OpenStreetMapsProvider } from './providers/open-street-maps/open-street-maps.provider';
import * as path from 'path';
import * as fs from 'fs';
import { pick } from 'stream-json/filters/Pick';
import { streamArray } from 'stream-json/streamers/StreamArray';
import { parser } from 'stream-json';
import { point, polygon } from '@turf/helpers';
import booleanPointInPolygon from '@turf/boolean-point-in-polygon';
import { Cache } from 'cache-manager';
import { first } from 'lodash';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AreaService {
  geocoder: ProviderAggregator;

  /**
   * Cache TTL
   */
  cacheTTL: number;

  constructor(
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    private configService: ConfigService,
  ) {
    const axiosInstance: AxiosInstance = axios.create({
      timeout: 5000,
    });
    const chainProvider: ChainProvider = new ChainProvider([
      new OpenStreetMapsProvider(axiosInstance),
      // new GoogleMapsProvider(axiosInstance, 'apikey'),
    ]);
    this.geocoder = new ProviderAggregator([chainProvider]);

    this.cacheTTL = 60 * 60 * 1000;
  }

  /**
   * Normalize address (uppercase, no duplicate whitespaces, etc.)
   * @param address
   */
  normalizeAddress(address: string): string {
    return address
      .split(' ')
      .filter((p) => p.length > 0)
      .map((p) => p.toUpperCase())
      .join(' ');
  }

  /**
   * Get area (geocode and look for overlapping polygons)
   * @param address
   */
  async getArea(address: string): Promise<ServiceAreaResponse> {
    let location: Location | undefined;
    try {
      location = await this.findLocation(address);
    } catch (err) {
      return {
        status: 'GEOCODING_FAILED',
        search: address,
      };
    }

    if (location === undefined) {
      return {
        status: 'ADDRESS_NOT_FOUND',
        search: address,
      };
    }

    const serviceArea = await this.findServiceArea({
      lat: location.latitude.toString(),
      lng: location.longitude.toString(),
    });

    if (!serviceArea) {
      return {
        status: 'NO_SERVICE_AREA',
        search: address,
      };
    }

    return {
      status: 'OK',
      search: address,
      location: {
        address1: location.street,
        postcode: location.postalCode,
        serviceArea,
        lng: location.longitude,
        lat: location.latitude,
        city: location.city,
      },
    };
  }

  /**
   * Find location using either cache (when useCache omitted or set to true)
   * @param address
   * @param useCache
   */
  async findLocation(
    address: string,
    useCache = true,
  ): Promise<Location | undefined> {
    const cacheKey = 'location/' + this.normalizeAddress(address);

    if (useCache) {
      const existing: Location | undefined = await this.cacheManager.get(
        cacheKey,
      );
      if (existing) {
        return existing;
      }
    }

    const locations: Location[] = await this.geocoder.geocode({
      address: this.normalizeAddress(address),
    });

    if (locations.length === 0) {
      return undefined;
    }

    const bestLocation: Location = first(locations);

    if (useCache) {
      await this.cacheManager.set(cacheKey, bestLocation, {
        ttl: this.cacheTTL,
      });
    }

    return bestLocation;
  }

  /**
   * Convert string or number coordinate to number or return undefined
   * when number would not be a valid coordinate.
   *
   * @param coord
   */
  coordToNumber(coord: string | number): number | undefined {
    let coordNumber: number;
    return (coordNumber = parseFloat(coord.toString())) <= 180
      ? coordNumber
      : undefined;
  }

  /**
   * Find service area by stream-reading and parsing GeoJSON file.
   * @param lat Latitude
   * @param lng Longitude
   * @param useCache
   */
  async findServiceArea(
    {
      lat,
      lng,
    }: {
      lat: string | number;
      lng: string | number;
    },
    useCache = true,
  ): Promise<string | undefined> {
    lng = this.coordToNumber(lng);
    lat = this.coordToNumber(lat);

    if (lng === undefined || lat === undefined) {
      throw new Error('Both coordinates must be valid numbers');
    }
    if (Math.abs(lat) > 90) {
      throw new Error('Latitude cannot be higher than 90');
    }

    const cacheKey = 'area/' + lat.toString() + '/' + lng.toString();

    if (useCache) {
      const existing: string | undefined = await this.cacheManager.get(
        cacheKey,
      );

      if (existing) {
        return existing;
      }
    }

    const serviceArea = await new Promise<string | undefined>((resolve) => {
      const pipeline = fs
        .createReadStream(
          path.resolve(
            process.cwd(),
            'assets',
            this.configService.get<string>('geojson.file'),
          ),
        )
        .pipe(parser())
        .pipe(pick({ filter: 'features' }))
        .pipe(streamArray());

      pipeline.on('error', (_err) => {
        resolve(undefined);
      });
      pipeline.on('data', (data) => {
        if (
          booleanPointInPolygon(
            point([lng as number, lat as number]),
            polygon(data.value.geometry.coordinates),
          )
        ) {
          resolve(data.value.properties.Name);
        }
      });
      pipeline.on('close', () => {
        resolve(undefined);
      });
    });

    if (serviceArea && useCache) {
      await this.cacheManager.set(cacheKey, serviceArea, {
        ttl: this.cacheTTL,
      });
    }

    return serviceArea;
  }
}
