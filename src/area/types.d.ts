interface ServiceAreaLocation {
  address1?: string;
  address2?: string;
  city?: string;
  lat: number;
  lng: number;
  serviceArea: string;
  postcode: string;
}

interface ServiceAreaResponseOk {
  status: 'OK';
  search: string;
  location?: ServiceAreaLocation;
}

interface ServiceAreaResponseNotFound {
  status:
    | 'NOT_FOUND'
    | 'ADDRESS_NOT_FOUND'
    | 'GEOCODING_FAILED'
    | 'NO_SERVICE_AREA';
  search: string;
}

type ServiceAreaResponse = ServiceAreaResponseOk | ServiceAreaResponseNotFound;
