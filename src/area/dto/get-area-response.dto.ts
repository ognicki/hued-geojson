import { ApiProperty } from '@nestjs/swagger';

export class GetAreaResponseLocationDto implements ServiceAreaLocation {
  @ApiProperty({ type: 'string' })
  lat: number;
  @ApiProperty({ type: 'string' })
  lng: number;
  @ApiProperty({ type: 'string' })
  postcode: string;
  @ApiProperty({ type: 'string' })
  serviceArea: string;
}

export class GetAreaResponseDto implements ServiceAreaResponseOk {
  @ApiProperty({ type: 'string' })
  status: 'OK';

  @ApiProperty({ type: GetAreaResponseLocationDto })
  location: GetAreaResponseLocationDto;

  @ApiProperty()
  search: string;
}
