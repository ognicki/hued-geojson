import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';

export class GetAreaRequestDto {
  @ApiProperty({
    type: 'string',
    description: 'Human-readable address',
  })
  @IsString()
  @MinLength(3)
  readonly address: string;
}
