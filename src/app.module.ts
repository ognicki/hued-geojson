import { CacheModule, Module } from '@nestjs/common';
import { AreaController } from './area/area.controller';
import { AreaService } from './area/area.service';
import { ConfigModule } from '@nestjs/config';
import geojsonConfig from './config/geojson.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [geojsonConfig],
    }),
    CacheModule.register(),
  ],
  controllers: [AreaController],
  providers: [AreaService],
})
export class AppModule {}
