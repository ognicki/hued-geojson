import { ApiProperty } from '@nestjs/swagger';

/**
 * Universal error response
 */
export class ErrorResponse {
  @ApiProperty({ type: 'number' })
  statusCode: number;

  @ApiProperty({ oneOf: [{ type: 'string' }, { type: 'string[]' }] })
  message?: string | string[];

  @ApiProperty()
  error: string;
}
