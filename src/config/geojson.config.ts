import { registerAs } from '@nestjs/config';

export default registerAs('geojson', () => ({
  file: process.env.GEOJSON_FILE || 'formatted-districts.json',
}));
